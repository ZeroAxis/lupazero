﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Rigidbody _rb;
    public float speed;
    public float lifetime;

    [HideInInspector]
    public Character owner;

    private void Awake()
    {
        _rb = this.GetComponent<Rigidbody>();
        GameObject.Destroy(this.gameObject, lifetime);
    }

    /// <summary>
    ///     Se activa al colisionar.
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        //Si colisiono contra un personaje le sacamos vida.
        var character = collision.gameObject.GetComponent<Character>();
        if(character)
        {
            if(character.TakeDamage(owner.damage))
            {
                owner.timeToComboKill = 5;
                owner.PlayComboSound();
            }
        }

        //Destruimos esta bala, sin importar contra qué colisionó.
        GameObject.Destroy(this.gameObject);
    }

    /// <summary>
    ///     Se ejecuta en cada iteración de la física.
    /// </summary>
    private void FixedUpdate()
    {
        _rb.velocity = this.transform.forward * speed;
    }
}
