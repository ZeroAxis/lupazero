﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXExplode : MonoBehaviour
{
	//Se ejecuta al inicializar...
	void Start ()
	{
        for (var i=0; i<this.transform.childCount; i++)
        {
            var rbChild = this.transform.GetChild(i).GetComponent<Rigidbody>();
            if(rbChild)
                rbChild.AddExplosionForce(1000, this.transform.position, 1000);
        }

        GameObject.Destroy(this.gameObject, 3);
	}
	
	//Se llama al pintar la pantalla...
	void Update ()
	{
		
	}
}
