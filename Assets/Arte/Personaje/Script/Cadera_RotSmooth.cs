﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cadera_RotSmooth : MonoBehaviour
{
    public GameObject Tracking_Cabeza;
    public GameObject AnclaCadera;
    public float Suavidad;
   

  

    // Update is called once per frame
    void Update()
    {

        RotacionSuave();
                                  
    }



    public void RotacionSuave()
    {
     

        AnclaCadera.transform.rotation = Quaternion.Slerp( new Quaternion(AnclaCadera.transform.rotation.x, Tracking_Cabeza.transform.rotation.y, AnclaCadera.transform.rotation.z, Tracking_Cabeza.transform.rotation.w),
                                                           new Quaternion(AnclaCadera.transform.rotation.x, AnclaCadera.transform.rotation.y, AnclaCadera.transform.rotation.z, AnclaCadera.transform.rotation.w), Suavidad);

    }
}
