﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class POSXZ : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform Personaje;
    public Transform TrackingCabeza;
   

    void Start()
    {
        Personaje.transform.position = new Vector3(TrackingCabeza.transform.position.x, Personaje.transform.position.y, TrackingCabeza.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        Personaje.transform.position = new Vector3(TrackingCabeza.transform.position.x, Personaje.transform.position.y, TrackingCabeza.transform.position.z);
    }
}
