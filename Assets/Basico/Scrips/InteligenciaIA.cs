﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class InteligenciaIA : MonoBehaviourPun
{
    public ControlPersonajeç Owner;
    public Transform puntocamara;
    public GameObject personaje;
    private float Tempo = 3;
    private void Awake()
    {
        Owner = this.gameObject.GetComponent<ControlPersonajeç>();

    }
    // Start is called before the first frame update
    void Start()
    {
        if (!photonView.IsMine)
        {
            gameObject.name = "Enemigovacio";
            Destroy(this);
            Destroy(GetComponent<Rigidbody>());
        }
        else
        {
            gameObject.name = "EnemigoMaster";
            personaje = GameObject.FindGameObjectWithTag("Player");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (personaje)
        {
            transform.LookAt(personaje.transform.position);
            //print(Vector3.Distance(gameObject.transform.position, personaje.transform.position));
            if (Vector3.Distance(gameObject.transform.position, personaje.transform.position) > 10f)
            {
                print("Persiguiendo soy "+gameObject.name);
              gameObject.transform.position= Vector3.MoveTowards(gameObject.transform.position, personaje.transform.position, 20 * Time.fixedDeltaTime);
                Tempo -= Time.time;
                if (Tempo <= 0)
                {
                    Owner.Disparo();
                }
            }
          

        }
    }
}
