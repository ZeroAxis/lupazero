﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Valve.VR;

public class ControlPersonajeGoblal : MonoBehaviourPun, IPunObservable

{
    public SteamVR_Action_Boolean Gatillo = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("GrabPinch");
 
    // Start is called before the first frame update

    public bool local;
    public GameObject VR;
    public GameObject ColisionadoresArma;
    public GameObject Personaje;
    public GameObject Cabezaphoton;
    public GameObject GafasVR;
   // public GameObject CabezaPersonaje;
    public GameObject Manoderecha;
    public GameObject ManoIzquierda;
    public GameObject Cadera;
    public Transform Pistoladerecha;
    public Transform PistolaIzquierda;

    // Sincronizadas
    public bool actderecha;
    public bool actizquierda;
    public bool PresionoGatillo;
    public bool Interactuable;
    //----------------

    public Transform PuntodedisparoD;
 
   

   

    void Start()
    {
        if (photonView.IsMine)
        {
            print("es local activo vr");
            ActivoPersona(true);
            gameObject.name = " AA VR";
            local = true;
        }
        else
        {
            print("NO es local activo desactivo vr");
            ActivoPersona(false);
            gameObject.name = " AA INTRUSO";
            local= false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine)
        {
            //print(Gatillo.);

       


                if (Gatillo.GetStateDown(SteamVR_Input_Sources.RightHand))
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    GameObject.Find("ControlJuego").GetComponent<ControlJuegoscrip>().EmpiezaEljuego = true;
                }


                print("presiono Gatillo");
                PresionoGatillo = true;
                //photonView.RPC("Disparo", RpcTarget.All);
            }//gatillo suelto
            if (Gatillo.GetStateUp(SteamVR_Input_Sources.RightHand))
            {
                PresionoGatillo = false;
            }

          
        }



       

            ActivacionManos();

    }


    public void ActivoPersona(bool act)
    {
        if (act)
        {
            // VR ACTIVO
            VR.SetActive(true);
           // Personaje.GetComponent<SkinnedMeshRenderer>().enabled = false;
            Cabezaphoton.transform.SetParent(GafasVR.transform);
            Cabezaphoton.transform.localPosition = Vector3.zero;
            Manoderecha.transform.SetParent(Pistoladerecha);
           // Manoderecha.transform.localPosition = Vector3.zero;
            ManoIzquierda.transform.SetParent(PistolaIzquierda);
            ManoIzquierda.transform.localPosition = Vector3.zero;


        }
        else
        {
            //Desactivo VR
            VR.SetActive(false);
            ColisionadoresArma.SetActive(false);
           
        
        }
    }

    public void ActivacionManos()
    {
        if (photonView.IsMine)
        {
            if (Pistoladerecha.localPosition != Vector3.zero)
            {
                actderecha = true;

            }
            else
            {

                actderecha = false;
            }


            if (PistolaIzquierda.localPosition != Vector3.zero)
            {
                actizquierda = true;
            }
            else
            {
                actizquierda = false;
            }
        }

        Manoderecha.SetActive(actderecha);
        ManoIzquierda.SetActive(actizquierda);


    }


    
    [PunRPC]
    public void Disparo()
    {
        if (!actderecha) return;
        PresionoGatillo = true;


        //print("disparo");
        //var bala = GameObject.Instantiate(Prebala);
        ////bala.transform.position = PuntodedisparoD.position;
        //bala.transform.position = Manoderecha.transform.position+ Manoderecha.transform.forward*0.5f;
        ////bala.transform.localPosition = PuntodedisparoD.position + PuntodedisparoD.forward*2;
        //bala.transform.forward = Manoderecha.transform.forward;

    }




    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            print("envio datos");
            stream.SendNext(actderecha);
            stream.SendNext(actizquierda);
            stream.SendNext(PresionoGatillo);
            stream.SendNext(Interactuable);
        }


        if (stream.IsReading)
        {
            print("recivo");
            this.actderecha = (bool)stream.ReceiveNext();
            this.actizquierda= (bool)stream.ReceiveNext();
            this.PresionoGatillo = (bool)stream.ReceiveNext();
            this.Interactuable = (bool)stream.ReceiveNext();
        }
    }


}
