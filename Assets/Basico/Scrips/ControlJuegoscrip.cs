﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class ControlJuegoscrip : MonoBehaviourPun
{
    public GameObject Enemigo;
    public int maxNumEnemigo = 3;
    public int NumEnemigo = 0;
    public GameObject[] SpawnE;
    public float tiempoSpawn;
    private float tiempodescuento;
    public bool EmpiezaEljuego=false;
    public bool ModoSinVR;


    public GameObject[] Players;
    // Start is called before the first frame update
    void Start()
    {

        //if (!PhotonNetwork.IsMasterClient) Destroy(this);

        SpawnE = GameObject.FindGameObjectsWithTag("SpawnPoint");
        tiempodescuento = tiempoSpawn;
    



    }

    // Update is called once per frame
    void Update()
    {

        if (!PhotonNetwork.IsMasterClient) return;

        #region Instanciacion Enemigos
        if (EmpiezaEljuego)
        {
            tiempodescuento -= Time.fixedDeltaTime;
            if (tiempodescuento <= 0)
            {
                if (NumEnemigo < maxNumEnemigo)
                {
                    NumEnemigo++;
                    int pos = Random.Range(0, SpawnE.Length);
                    PhotonNetwork.Instantiate(Enemigo.name, SpawnE[pos].transform.position, Quaternion.identity, 0);
                    print("Instancio Enemigo");

                }

                tiempodescuento = tiempoSpawn;

            }
        }
        #endregion




    }


    public int NumerodePlayers()
    {
        Players = GameObject.FindGameObjectsWithTag("Player");
        return Players.Length;
    }

    //public int NumeroPlayersInteractivos()
    //{
    //    int numero=0;
    //    Players = GameObject.FindGameObjectsWithTag("Player");

    //    if (!ModoSinVR)
    //    {
    //        foreach (GameObject perso in Players)
    //        {
    //            if (perso.transform.root.GetComponent<ControlPersonajeGoblal>().Interactuable) numero++;
    //        }
    //    }
    //    else
    //    {
    //        foreach (GameObject perso in Players)
    //        {
    //            if (perso.GetComponent<PlayerControlPersonaje>().Interactuable) numero++;
    //        }

    //    }
           

    //    return numero;
    //}


    public void zonas()
    {

    }
}
