﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerControlPersonaje : MonoBehaviourPun,IPunObservable
  
{
    public ControlPersonajeç Owner;
    public Transform puntocamara;
    public bool local;
    public bool Interactuable;
    private void Awake()
    {
        Owner = this.gameObject.GetComponent<ControlPersonajeç>();

    }
    // Start is called before the first frame update
    void Start()
    {
        if (!photonView.IsMine) 
        {
           // Destroy(this);
           // Destroy(GetComponent<Rigidbody>());
        }
        else
        {
            gameObject.name = "Personaje Local";
            Camera.main.transform.SetParent(puntocamara, true);
            local = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!photonView.IsMine)
        {
            return;
        }

            if (Input.GetButtonDown("Jump")) Owner.Salta();
        if (Input.GetButton("Horizontal"))
        {

            Owner.Giro(Input.GetAxis("Horizontal") > 0 ? 1 : -1);

        }
        if (Input.GetButton("Vertical"))
        {

            Owner.Muevete(Input.GetAxis("Vertical") > 0 ? 1 : -1);

        }

        if (Input.GetButtonDown("Fire1"))
        {
            photonView.RPC("Disparo",RpcTarget.All);
        }
    }



    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
        
            stream.SendNext(Interactuable);
        }


        if (stream.IsReading)
        {

            this.Interactuable = (bool)stream.ReceiveNext();
        }
    }

}
