﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Photon.Pun;
using System.Linq;

public class Controller_Parasite : MonoBehaviourPun
{
    public Animator Ani;
    public NavMeshAgent Nav;
    public GameObject [] TodosJugadores;
    public GameObject JugadorCercano;
    public float Vida = 100;
    public bool Recalculo = false;
    public bool muerto = false;
    public Material Disolvente;
    public GameObject Bicho;
    // Start is called before the first frame update


  

    void Start()
    {
        //TodosJugadores = GameObject.FindGameObjectsWithTag("Player");


        if (!photonView.IsMine)
        {
            gameObject.name = "ParasitoVacio";
            Destroy(Nav);
           // Destroy(this);

        }
        else
        {

            TodosJugadores = GameObject.FindGameObjectsWithTag("Player");
            BuscoJugadorMasCercano();
            gameObject.name = "ParasitoMaster";
            Nav.SetDestination(JugadorCercano.transform.position);
            //personaje = GameObject.FindGameObjectWithTag("Player");
            Nav.stoppingDistance = 1;
        }

    }

    // Update is called once per frame
    void Update()
    {
        

       

       if (!photonView.IsMine) return;
       
      
        if (Vida > 0)
        {
            Nav.SetDestination(JugadorCercano.transform.position);

            if (Nav.remainingDistance <= Nav.stoppingDistance)
            {
                print("atacando;");
                Ani.SetBool("Ataque",true);
                Ani.SetBool("Herido",false);
                Ani.SetBool("Herido",false);
                Nav.speed=0;


                //print("atacando : "+ Nav.remainingDistance);
                Recalculo = true;
            }
            else
            {

                if (Recalculo) RecalculoJugador();
              
                Ani.SetBool("Ataque", false);
              
                if (Vida <= 50) Ani.SetBool("Herido", true);
                if (Vida > 50) Ani.SetBool("correr", true);
              
            }
          
            

          
           
        }
        else
        {



            if (!muerto)
            {
                Ani.SetBool("Muerto", true);
                Nav.Stop();
                photonView.RPC("muerte", RpcTarget.All);
                //GameObject.Find("ControlJuego").GetComponent<ControlJuegoscrip>().NumEnemigo--;
                muerto = true;
            }
           

        }
      

    }

    [PunRPC]
    public void muerte()
    {
   
        Destroy(GetComponent<BoxCollider>());
        Destroy(gameObject, 5f);
    }

    public void BuscoJugadorMasCercano()
    {
        JugadorCercano=TodosJugadores.OrderBy(JugadorCercano => Vector3.Distance(transform.position, JugadorCercano.transform.position)).FirstOrDefault();
          
    }


    public void RecalculoJugador()
    {
        print("RecalculoJugador");
        BuscoJugadorMasCercano();
        Nav.speed = 3.5f;
        Recalculo =false;
    }

    public void RecivoDaño(float Daño)
    {
        if (!photonView.IsMine) return;

        Vida -= Daño;

    }

  
}
