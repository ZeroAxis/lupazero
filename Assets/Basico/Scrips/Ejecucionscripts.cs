﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Experimental.XR;
using UnityEngine.VR;

public class Ejecucionscripts : MonoBehaviour
{
    bool useNonVrPlayerInEditor;
    public Text inf;

    public void Awake()
    {
#if UNITY_EDITOR

        inf.text = "Modo Editor";

        if (UnityEngine.XR.XRSettings.loadedDeviceName == "Oculus")
        {
            inf.text = "/n TENGo OCulus";
        }
        else
        {
            inf.text = "/n  Steam";
        }

        if (useNonVrPlayerInEditor)
        {
            //_playerPrefab = _nonVrPlayerPrefab;
            //return;
        }
#else
            
         inf.text = "Modo jUEGO";
        useNonVrPlayerInEditor = false;
#endif

#if UNITY_STANDALONE
        if (UnityEngine.XR.XRSettings.loadedDeviceName == "OpenVR")
        {
            inf.text = "/n Tengo las HTC VIVE puestas";
        }
        else
        {
            inf.text = "/n  Tengo una mierda puestas";
        }
#endif
        //#elif UNITY_HAS_GOOGLEVR && (UNITY_ANDROID || UNITY_EDITOR)

        //            var loadedDevice = UnityEngine.XR.XRSettings.loadedDeviceName;
        //            _playerPrefab = _googleVrPlayerPrefabCardboard;

        //            if (string.Equals(loadedDevice, "Daydream", StringComparison.OrdinalIgnoreCase))
        //                _playerPrefab = _googleVrPlayerPrefabDaydream;

        //            if (string.Equals(loadedDevice, "Oculus", StringComparison.OrdinalIgnoreCase))
        //                _playerPrefab = _gearPlayerPrefab;
        //#endif
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
