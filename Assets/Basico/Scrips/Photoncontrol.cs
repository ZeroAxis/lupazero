﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.XR;

public class Photoncontrol : MonoBehaviourPunCallbacks
{
    private void Awake()
    {
        if (!Application.isEditor)
        {
            //XRSettings.enabled = false;
           // InputTracking.disablePositionalTracking = false;
        }
    }




    public Transform Puntoinstancio;
    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.GameVersion = "1";

        //PhotonNetwork.load
        PhotonNetwork.ConnectUsingSettings();   
        //primer evento para la conexion a photon q llamara
        // (1)  ala funciona onconnectedtophoton    
    }



    public override void OnConnected() //regla fundamental de phootn , no hace falta llamar ala clase base
    {
        //(2) despues de conectarse se conectara al servidor de photon
        print("conectado ha photon");
    }



    public override void OnConnectedToMaster()
    {
        //(3) Conectado al servidor
        print("Conectado al servidor");
        //Momento preciso en el cual se puede crear un LOBBY


        //FUNCION QUE  crea o si ya esta creada se une ala sala
        // Primer parametro nombre sala
        // Segundo parametro parametros de configuracion  creamos la variable room option
        // que para crear este parametro necesitamos a photon.real
        var confSala = new RoomOptions();
        confSala.MaxPlayers = 10;
        // Tercera opcion , el tipo de lobby creada

        PhotonNetwork.JoinOrCreateRoom("Ojete",confSala,TypedLobby.Default);
        // SE GENERA 2 eventos 1 OncreateRoom o OnJoinedRoom

    }


    public override void OnCreatedRoom()
    {
        print(" se general La Sala");
    }

    public override void OnJoinedRoom()
    {
        print( " Se une ala sala ");
        //Al entrar la sala se instancia el objeto
       


        if (UnityEngine.XR.XRSettings.loadedDeviceName == "OpenVR")
        {
          
            Vector3 posi = new Vector3(Puntoinstancio.position.x, 0, Puntoinstancio.position.z);
            PhotonNetwork.Instantiate("PersonajeGlobal", posi, Quaternion.identity, 0);
            print("personajeGloballl");
        }
        else
        {


           
            Vector3 po = new Vector3(0, 1, 0);
            PhotonNetwork.Instantiate("Personaje", po, Quaternion.identity, 0);
        }


    }


}
