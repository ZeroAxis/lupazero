﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class ControlPersonajeç : MonoBehaviourPun,IPunObservable
{
    public Rigidbody _rb;
    public GameObject PreBala;
    public Transform Puntodisparo;
    public float Vida = 100;
    public RectTransform barravida;
    private void Awake()
    {
        _rb = this.GetComponent<Rigidbody>();
    }
    // Start is called before the first frame update
    void Start()
    {
        //Physics.IgnoreCollision(bullet.GetComponent<Collider>(), this.GetComponent<Collider>(), true);
    }

    public void Muevete(float dire)
    {
        var realVelocidad= dire * 100 * Time.fixedDeltaTime;
       transform.Translate(Vector3.forward*dire*10*Time.fixedDeltaTime);
    }
    private void Update()
    {
        barravida.sizeDelta = new Vector2(Vida * 4, barravida.sizeDelta.y);
    }

    public void Daño(float san)
    {
        if (photonView.IsMine)
        {
            print("Recibo Daño");
            Vida -= san;
           
            Muerto();
        }
      
    }

    public void Giro(float rotation)
    {
        gameObject.transform.Rotate(Vector3.up * rotation * 200 * Time.fixedDeltaTime);
    }


    public void Muerto()
    {
        if (Vida <= 0)
        {
            var enemigo = gameObject.GetComponent<InteligenciaIA>();
            if (enemigo)
            {
                print("Muere enemigo instancio otro");
                PhotonNetwork.Destroy(gameObject);
                GameObject.Find("ControlJuego").GetComponent<ControlJuegoscrip>().NumEnemigo--;
            }
            else
            {
                gameObject.transform.position = new Vector3(0, 1, 0);
                Vida = 100;
            }
            
        }
    }

    [PunRPC]
    public void Disparo()
    {
        print("disparo");
        var bala = GameObject.Instantiate(PreBala);
        bala.transform.position = gameObject.transform.position + gameObject.transform.forward * 2;

        bala.transform.forward = gameObject.transform.forward;

    }

    public void Salta()
    {
        _rb.AddForce(Vector3.up * 5, ForceMode.Impulse);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(Vida);
        }


        if (stream.IsReading)
        {

            Vida = (float)stream.ReceiveNext();
        }
    }
 
}
