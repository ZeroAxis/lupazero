﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{
    public Rigidbody _rb;
    // Start is called before the first frame update
    private void Awake()
    {
        _rb = gameObject.GetComponent<Rigidbody>();
    }
    void Start()
    {
        Destroy(gameObject, 2f);
    }

    // Update is called once per frame
    public void FixedUpdate()
    {
        _rb.velocity = this.transform.forward * 2000 * Time.fixedDeltaTime;
    }


    private void OnCollisionEnter(Collision collision)
    {
        print(collision.gameObject.name);

        var Jugador = collision.gameObject.GetComponent<Controller_Parasite>();
        if (Jugador)
        {
            Jugador.RecivoDaño(60);
        }
        Destroy(gameObject);

    }

}
