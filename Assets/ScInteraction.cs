﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ScInteraction : MonoBehaviour
{
    public ControlPersonajeGoblal control;
    public SteamVR_Action_Boolean Empuñadura = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("GrabPinch");
  
    public GameObject interactivo;
    // Start is called before the first frame update

    private void Update()
    {
        if (Empuñadura.GetStateDown(SteamVR_Input_Sources.RightHand))
        {
         if(interactivo)interactivo.GetComponent<Botonera>().CAMBIOIDENTIDAD();
        }
     
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Interracion")
        {
            other.gameObject.GetComponent<Botonera>().Resplandor.enabled = true;
            interactivo = other.gameObject;

        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Interracion")
        {
            other.gameObject.GetComponent<Botonera>().Resplandor.enabled = false;
            interactivo = null;
        }
    }

    private void OnTriggerStay(Collider other)
    {
       // if (other.gameObject.tag == "Interracion" && control.PresionoEmpunadura) other.gameObject.GetComponent<Botonera>().CAMBIOIDENTIDAD();
    }
}
