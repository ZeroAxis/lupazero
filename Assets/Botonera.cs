﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Botonera : MonoBehaviour
{
   public Material VERDE;
    public Material ROJO;
    public bool Act = false;
    public GameObject boton;
    public Outline Resplandor;

   

    // Start is called before the first frame update
    void Start()
    {
        Resplandor.enabled = false;
      
    }

    // Update is called once per frame
    void Update()
    {

        if (Act)
        {

            boton.GetComponent<MeshRenderer>().material = VERDE;
        }
        else
        {
            boton.GetComponent<MeshRenderer>().material = ROJO;
        }
    }

  


    public void mandoseñal()
    {
        PhotonView photonView = PhotonView.Get(this);
        photonView.RPC("CAMBIOIDENTIDAD", RpcTarget.All);
    }


    [PunRPC]
    public void CAMBIOIDENTIDAD()
    {
        if (Act)
        {
            Act = false;
            boton.GetComponent<MeshRenderer>().material = VERDE;
        }
        else
        {
            Act = true;
            boton.GetComponent<MeshRenderer>().material = ROJO;
        }
    }

    
}
